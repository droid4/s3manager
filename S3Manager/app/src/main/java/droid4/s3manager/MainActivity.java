package droid4.s3manager;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;


public class MainActivity extends AppCompatActivity {
    private static final String LOG_TAG = MainActivity.class.getName();
    private static final String KEY_NAV_SELECTED_ID = "NavSelectedId";

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            return selectFragment(item);
        }

    };

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        if(navigation != null) {
            int selectedId = navigation.getSelectedItemId();
            android.util.Log.d(LOG_TAG, "<<< selecteId = " + selectedId);
            if(selectedId > 0) {
                outState.putInt(KEY_NAV_SELECTED_ID, selectedId);
            }
        }

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        if(navigation != null) {
            navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

            int selectedId = 0;
            if(savedInstanceState != null) {
                android.util.Log.d(LOG_TAG, ">>> savedInstanceState is not null.");
                int selId = savedInstanceState.getInt(KEY_NAV_SELECTED_ID);
                android.util.Log.d(LOG_TAG, ">>> selId = " + selId);
                if(selId > 0) {  // ???
                    selectedId = selId;
                }
            }
            android.util.Log.d(LOG_TAG, ">>> selecteId = " + selectedId);
            if(selectedId == 0) {
                selectedId = R.id.navigation_file_download;  // Select the first one.
            }

            MenuItem previouslySelectedItem = findMenuItem(selectedId);
            if(previouslySelectedItem != null) {
                selectFragment(previouslySelectedItem);
            }
        }
    }

    private MenuItem findMenuItem(int itemId) {
        MenuItem menuItem = null;
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        if (navigation != null) {
            Menu menu = navigation.getMenu();
            menuItem = menu.findItem(itemId);
        }
        return menuItem;
    }

    private boolean selectFragment(@NonNull MenuItem item) {
        item.setChecked(true);

        switch (item.getItemId()) {
            case R.id.navigation_file_download:
                pushFragment(new FileDownloadFragment());
                return true;
            case R.id.navigation_file_upload:
                pushFragment(new FileUploadFragment());
                return true;
//            case R.id.navigation_file_list:
//                pushFragment(new FileListFragment());
//                return true;
            case R.id.navigation_settings:
                Intent settingsIntent = new Intent(this, SettingsActivity.class);
                startActivity(settingsIntent);
                return true;
        }
        return false;
    }

    private void pushFragment(Fragment fragment) {
        if (fragment == null) {
            return;
        }
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager != null) {
            FragmentTransaction ft = fragmentManager.beginTransaction();
            if (ft != null) {
                ft.replace(R.id.content, fragment);
                ft.commit();
            }
        }
    }

}
