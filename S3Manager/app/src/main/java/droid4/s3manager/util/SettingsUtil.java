package droid4.s3manager.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.text.TextUtils;

/**
 * Created by harry on 9/10/17.
 */

public final class SettingsUtil {
    private static String LOG_TAG = SettingsUtil.class.getName();
    private SettingsUtil() {}

    public static boolean isDefaultBucketSet(@NonNull Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String defaultBucket = prefs.getString("s3_bucket_name", "");
        if(TextUtils.isEmpty(defaultBucket)) {
            return false;
        } else {
            return true;
        }
    }

}
