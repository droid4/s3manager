package droid4.s3manager.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;

import droid4.s3manager.R;

/**
 * Created by harry on 9/9/17.
 */

public final class AwsClientUtil {
    private static String LOG_TAG = AwsClientUtil.class.getName();
    private AwsClientUtil() {}

    // tbd:
    // Cache AmazonS3Client instances (per access/secret key pair)???
    // ...


    public static boolean hasAwsCredentials(@NonNull Context context) {
        String accessKey = "";
        String secretKey = "";
        if(context.getResources().getBoolean(R.bool.use_config_aws_credentials)) {
            // Read them from aws-credentials.xml
            accessKey = context.getResources().getText(R.string.access_key).toString();
            secretKey = context.getResources().getText(R.string.secret_key).toString();
        } else {
            // Read access/secret keys from preferences.
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            accessKey = prefs.getString("aws_access_key", "");
            secretKey = prefs.getString("aws_secret_key", "");
        }
        return (! TextUtils.isEmpty(accessKey) && ! TextUtils.isEmpty(secretKey));
    }

    public static AmazonS3 createAmazonS3Client(@NonNull Context context) {
        String accessKey = "";
        String secretKey = "";
        if(context.getResources().getBoolean(R.bool.use_config_aws_credentials)) {
            // Read them from aws-credentials.xml
            accessKey = context.getResources().getText(R.string.access_key).toString();
            secretKey = context.getResources().getText(R.string.secret_key).toString();
        } else {
            // Read access/secret keys from preferences.
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            accessKey = prefs.getString("aws_access_key", "");
            secretKey = prefs.getString("aws_secret_key", "");
        }
        // For debugging only.
        // TBD: Remove this before release !!!
        android.util.Log.d(LOG_TAG, ">>> accessKey = " + accessKey);
//        android.util.Log.d(LOG_TAG, ">>> secretKey = " + secretKey);
        // ...

        return AwsClientUtil.createAmazonS3Client(accessKey, secretKey);
    }

    public static AmazonS3 createAmazonS3Client(@NonNull String accessKey, @NonNull String secretKey) {

        // ???
        BasicAWSCredentials creds = new BasicAWSCredentials(accessKey, secretKey);
//        AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(creds)).build();
        AmazonS3 s3Client = new AmazonS3Client(creds);

        return s3Client;
    }

}
