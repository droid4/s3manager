package droid4.s3manager.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3;

/**
 * Created by harry on 9/9/17.
 */

public final class S3TransferUtil {
    private static String LOG_TAG = S3TransferUtil.class.getName();
    private S3TransferUtil() {}

    // Cf. http://docs.aws.amazon.com/AWSAndroidSDK/latest/javadoc/com/amazonaws/mobileconnectors/s3/transferutility/TransferUtility.html
    public static TransferUtility getTransferUtility(@NonNull Context context) {

        AmazonS3 s3Client = AwsClientUtil.createAmazonS3Client(context);

        TransferUtility.Builder builder = TransferUtility.builder().s3Client(s3Client).context(context);
        // Set default bucket here..
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String defaultBucket = prefs.getString("s3_bucket_name", "");
        if(!TextUtils.isEmpty(defaultBucket)) {
            builder.defaultBucket(defaultBucket);
        }
        TransferUtility transferUtility = builder.build();

        return transferUtility;
    }
}
