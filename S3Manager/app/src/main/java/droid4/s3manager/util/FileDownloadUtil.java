package droid4.s3manager.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.text.TextUtilsCompat;
import android.text.TextUtils;

import java.io.File;

/**
 * Created by harry on 9/9/17.
 */

public final class FileDownloadUtil {
    private static String LOG_TAG = FileDownloadUtil.class.getName();
    private FileDownloadUtil() {}


    public static String getDefaultDownloadLocation(@NonNull Context context) {
        String filePath = null;

        // Read it from preferences
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        filePath = prefs.getString("default_download_location", "");
        if(! TextUtils.isEmpty(filePath) && ! filePath.startsWith(File.separator)) {
            filePath = File.separator + filePath;
        }
        File file1 = new File(filePath);
        if(TextUtils.isEmpty(filePath) || !file1.exists() || !file1.isDirectory()) {
            // temporary
            // tbd: check at least if there is an sdcard mounted ???
            filePath = Environment.getExternalStorageDirectory().getAbsolutePath();
            File file2 = new File(filePath);
            if (!file2.exists() || !file2.isDirectory()) {
                // ???
                filePath = "/mnt/sdcard";
            }
        }
        // ...

        return filePath;
    }

    public static String getDownloadFilePath(@NonNull Context context, String name) {
        // temporary
        // validate, etc.....
        String path = getDefaultDownloadLocation(context);
        if(! path.endsWith(File.separator)) {
            path += File.separator;
        }
        // if(name.endsWith(File.separator)) {} // error..
        int lidx = name.lastIndexOf(File.separator);
        if(lidx >= 0) {
            name = name.substring(lidx + 1);
        }
        path += name;
        return path;
    }

}
