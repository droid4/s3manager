package droid4.s3manager;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;

import droid4.s3manager.util.AwsClientUtil;
import droid4.s3manager.util.FileUploadUtil;
import droid4.s3manager.util.S3TransferUtil;
import droid4.s3manager.util.SettingsUtil;

import static android.R.attr.path;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FileUploadFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FileUploadFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FileUploadFragment extends Fragment {
    private static String LOG_TAG = FileUploadFragment.class.getName();
    private static int REQUEST_CODE_FILE_PICK = 1001;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;


    private AmazonS3 mS3Client = null;
    private AmazonS3 getS3Client() {
        if (mS3Client == null) {
            mS3Client = AwsClientUtil.createAmazonS3Client(getActivity());
        }
        return mS3Client;
    }
    private TransferUtility mTransferUtility = null;
    private TransferUtility getTransferUtility() {
        if(mTransferUtility == null) {
            mTransferUtility = S3TransferUtil.getTransferUtility(getActivity());
        }
        return mTransferUtility;
    }


    public FileUploadFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FileUploadFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FileUploadFragment newInstance(String param1, String param2) {
        FileUploadFragment fragment = new FileUploadFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_file_upload, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

//        // Reset the status view.
//        TextView statusView = getActivity().findViewById(R.id.fileUploadStatus);
//        if(statusView != null) {
//            statusView.setText("");
//        }

        final Button buttonFileUpload = getActivity().findViewById(R.id.buttonFileUpload);
        if (buttonFileUpload != null) {
            buttonFileUpload.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            android.util.Log.d(LOG_TAG, "buttonFileUpload clicked.");

                            // Clear the status view first.
                            TextView statusView = getActivity().findViewById(R.id.fileUploadStatus);
                            if(statusView != null) {
                                statusView.setText("");
                            }

                            if(! AwsClientUtil.hasAwsCredentials(getActivity())) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setTitle(R.string.query_dialog_aws_credentials_missing)
                                        .setMessage(R.string.query_dialog_aws_credentials_question)
                                        .setPositiveButton(R.string.set_aws_credentials, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                Intent settingsIntent = new Intent(getActivity(), SettingsActivity.class);
                                                startActivity(settingsIntent);
                                            }
                                        })
                                        .setNegativeButton(R.string.cancel_query_dialog, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                // Just close the dialog.
                                            }
                                        });
                                // Create the AlertDialog object.
                                AlertDialog dialog = builder.create();
                                dialog.show();
                            } else {
                                String bucket = null;
                                EditText viewBucket = getActivity().findViewById(R.id.editUploadFileBucket);
                                if (viewBucket != null) {
                                    bucket = viewBucket.getText().toString();
                                }
                                android.util.Log.d(LOG_TAG, ">>> Upload bucket = " + bucket);
                                if (TextUtils.isEmpty(bucket) && !SettingsUtil.isDefaultBucketSet(getActivity())) {
                                    Toast.makeText(getActivity(), "Need to specify the bucket name.", Toast.LENGTH_SHORT).show();
                                } else {
                                    Intent intent = new Intent();
                                    intent.setType("*/*");
                                    intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
                                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                                    String[] mimetypes = {"audio/*", "video/*", "text/*"};
                                    intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
                                    startActivityForResult(intent, REQUEST_CODE_FILE_PICK);
                                }
                            }
                        }
                    }
            );
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_FILE_PICK
                && resultCode == Activity.RESULT_OK && data != null) {
            Uri uri = data.getData();
            if (uri != null) {
                File file = null;
                try {
                    String path = FileUploadUtil.getPath(getActivity(), uri);
                    android.util.Log.d(LOG_TAG, ">>> Upload file path = " + path);
                    file = new File(path);
                } catch (URISyntaxException e) {
                    android.util.Log.d(LOG_TAG, "exception: " + e);
                }

                if(file != null && file.exists()) {

                    String bucket = null;
                    EditText viewBucket = getActivity().findViewById(R.id.editUploadFileBucket);
                    if(viewBucket != null) {
                        bucket = viewBucket.getText().toString();
                    }
                    String folder = null;
                    EditText viewFileFoler = getActivity().findViewById(R.id.editUploadFileFolder);
                    if(viewFileFoler != null) {
                        folder = viewFileFoler.getText().toString();
                    }
                    String key = null;
                    EditText viewFileName = getActivity().findViewById(R.id.editUploadFileName);
                    if(viewFileName != null) {
                        key = viewFileName.getText().toString();
                    }
                    if(TextUtils.isEmpty(key)) {
                        key = file.getName();
                    }
                    if(!TextUtils.isEmpty(folder)) {
                        // Note: it's not File.separator. We just use "/".
                        if(! folder.startsWith("/")) {
                            folder = "/" + folder;   // Is this necessary ???
                        }
                        if(! folder.endsWith("/")) {
                            folder += "/";
                        }

                        if(key.startsWith("/")) {
                            key = key.substring(1);
                        }

                        // Key is the combination of "folder" and the file name.
                        key = folder + key;
                    }

                    TransferObserver observer = null;
                    if(TextUtils.isEmpty(bucket)) {
                        observer = getTransferUtility().upload(key, file);
                    } else {
                        observer = getTransferUtility().upload(bucket, key, file);
                    }
                    observer.setTransferListener(new TransferListener() {
                        @Override
                        public void onStateChanged(int id, TransferState state) {
                            android.util.Log.d(LOG_TAG, "onStateChanged() id = " + id + "; state = " + state);

                            // tbd.
//                                        TextView viewState = getActivity().findViewById(R.id.fileUploadState);
                            String status = "";
                            if (TransferState.COMPLETED.equals(state)) {
                                status = "Upload complete.";
                            } else if (TransferState.CANCELED.equals(state) || TransferState.PAUSED.equals(state)) {
                                status = "Upload interrupted.";
                            } else if (TransferState.FAILED.equals(state)) {
                                status = "Upload failed.";
                            } else {
                                // ignore. Could be in_progress...
                            }
                            if(! TextUtils.isEmpty(status)) {
//                                        viewState.setText(status);
                                Toast.makeText(getActivity(), status, Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                            android.util.Log.d(LOG_TAG, "onProgressChanged() id = " + id + "; bytesCurrent = " + bytesCurrent + "; bytesTotal = " + bytesTotal);

                        }

                        @Override
                        public void onError(int id, Exception e) {
                            android.util.Log.d(LOG_TAG, "onError() id = " + id + "; e = " + e);

                            TextView errorView = getActivity().findViewById(R.id.fileUploadStatus);
                            if(errorView != null) {
                                String msg = e.getMessage();
                                errorView.setText(msg);
                            }
                        }
                    });

                } else {
                    // ???
                    android.util.Log.d(LOG_TAG, "File does not exist. uri = " + uri);
                }
            } else {
                // ???
            }
        } else {
            // ...
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
