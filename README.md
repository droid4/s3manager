# S3 Manager

> Amazon S3 file upload/download utility app.

Live demo app available on Amazon App Store:
* [S3 Manager](https://www.amazon.com/dp/B075KRWNRP/)



## How to Use the App

S3 Manager is a very simple app that lets you upload/download files to/from Amazon S3 on a mobile device. Currently, it provides no other functions.

* Note 1. This is intended for developers. You need to have an AWS developer account in order to be able to use this app.
* Note 2. Your device needs to have an external storage ("sdcard").

### Getting Started with S3 Manager

1. First, you will need an AWS account, and access/secret keys. We reccommend to create a new (disposable) key pairs for a non-root account. 
1. Open Settings page, and copy and paste your key pairs under AWS General settings.
1. Optionally, you can save a default bucket name (for both upload and download). The bucket needs to exist on S3.
1. You can also set a specific download location on your device (e.g., /mnt/sdcard/download, etc.). Otherwise the files will be downloaded to a default folder (like "/mnt/sdcard"), which may vary from device to device.

### How to Download a File

1. Specify a bucket name. If it's not specified, the default value set in the settings, if any, will be used.
1. Specify the S3 file name. The file name can be in the form of a "path" (e.g., /abc/xyz). If so, you will need to specify the entire path.
1. Press the "Download" button. If successful, you can find the file under the download location.

### How to Upload a file

1. Specify a bucket name. If it's not specified, the default value set in the settings, if any, will be used.
1. Specify a "folder" path (e.g., /abc/xyz), if needed. The folder needs to exist on S3. If specified, the file name on S3 will be prepended by this "folder" path.
1. Specify a file name on S3. If a file name is not provided, the name of the uploaded file (on the device) will be used.
1. Presse the "Upload" button. This will open a file selection UI, and once you choose a file, it'll be uploaded to S3.


## How to Build

    gradle build


(Before building, you may need to comment out APK signing in the gradle build file, or at least add your own signing configs.)



## Refererences


* [AWS Mobile SDK](https://aws.amazon.com/mobile/sdk/)
* [AWS Mobile SDK for Android Developer Guide](https://docs.aws.amazon.com/mobile/sdkforandroid/developerguide/)
* [AWS SDK for Android](https://github.com/aws/aws-sdk-android)
* [AWS Mobile SDK for Android Samples](https://github.com/awslabs/aws-sdk-android-samples)
* [Set Up the AWS Mobile SDK for Android](http://docs.aws.amazon.com/mobile/sdkforandroid/developerguide/setup.html)
* [Store and Retrieve Files with Amazon S3](http://docs.aws.amazon.com/mobile/sdkforandroid/developerguide/s3transferutility.html)
* [Running S3TransferUtility Sample](https://github.com/awslabs/aws-sdk-android-samples/tree/master/S3TransferUtilitySample)


